## 插页式广告接入指南

### 前提条件

- 在接入之前请仔细阅读 [配置指南](https://gitee.com/sinagamesdk/overseas_sdk/blob/master/ad/%E4%BD%BF%E7%94%A8%E9%85%8D%E7%BD%AE%E6%8C%87%E5%8D%97.md)以及[初始化指南](https://gitee.com/sinagamesdk/overseas_sdk/blob/master/ad/SDK%E5%88%9D%E5%A7%8B%E5%8C%96%E6%8E%A5%E5%85%A5%E6%8C%87%E5%8D%97.md)中的相关操作。

### 正式接入

 1. 实例化GameInterstitialAd对象

 ```
    /**
     * 实例化对象
     * @param activity:当前类
     * @param adId:广告位ID
     */
   GameInterstitialAd interstitialAd = new GameInterstitialAd(activity,adId);

```


  2. 加载插页式广告
 ```

   /**
     * 加载插页式广告
     * @param activity
     */
    interstitialAd.loadInterstitialAd(Activity activity);

```
 3. 展示插页式广告
   ```

     /**
     * 展示插屏广告
     * @param activity
     */
    interstitialAd.showInterstitialAd(Activity activity);

   ```
   4. 设置监听事件
   
    通过设置监听事件可以获得插页式广告的各个生命周期，实现所需方法。具体实现方法如下：
```

  //设置插页式广告监听 

interstitialAd.setInterstitialListener(interstitialAdListener);

   //插页式广告回调声明
 private IEverInterstitialAdListener interstitialAdListener = new IEverInterstitialAdListener() {
         /**
         * 加载插页广告
         * @param className:广告名称
         */
        @Override
        public void onInterstitialAdLoaded(String className) {
            Log.e(TAG,"className---" + className);
        }

        /**
         * 插页广告加载失败
         * @param errorCode:错误码
         * @param errorMsg:错误信息
         */
        @Override
        public void onInterstitialLoadFailed( String s, String s1) {
           
        }

        /**
         * 点击插屏广告
         */
        @Override
        public void onClickInterstitialAd() {

        }

        /**
         * 关闭插屏广告
         */
        @Override
        public void onInterstitialAdClose() {
            
        }


    };

```
 5. 预加载

 如果需要自动加载下一条插页式广告，请在onInterstitialAdClose回调方法内调用load方法，具体代码如下：
```

        @Override
        public void onInterstitialAdClose() {
              interstitialAd.loadInterstitialAd(activity);
        }

```