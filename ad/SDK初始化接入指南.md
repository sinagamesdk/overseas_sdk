## SDK初始化接入指南

### 前提条件

请仔细阅读[配置指南](https://gitee.com/sinagamesdk/overseas_sdk/blob/master/ad/%E4%BD%BF%E7%94%A8%E9%85%8D%E7%BD%AE%E6%8C%87%E5%8D%97.md)中的相关操作

### 正式接入

  1 初始化
   （请在入口类的onCreate方法中调用initAdSDK进行初始化），详细代码如下：

  ```
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.start_layout);
        
        AdEverSDK.getInstance().setAdSystemListener(systemListener);
        AdEverSDK.getInstance().initAdSDK(this, appKey);
    }
 ```
 
  2 设置监听事件
    （请在入口类中注册以下监听事件，用来监听初始化是否成功）
  ```
    private IAdEverSystemListener systemListener = new IAdEverSystemListener() {
        @Override
        public void initAdSuccess() {
            
        }

        @Override
        public void initAdFailed(String s) {

        }


    };
```