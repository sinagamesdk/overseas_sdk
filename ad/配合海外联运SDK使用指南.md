如果您的应用在接入广告SDK的同时，也接入了新浪游戏的联运SDK，则需要做以下配置 

 **注意，以下配置请在调用广告SDK初始化之前进行**

 1、调用是否接入联运SDK接口,具体代码如下:


 **若接入过海外发行SDK此方法可不调用** 


 ```
       /**
         *
         *是否接入了海外联运SDK
         * @params isExit:默认为true，接入过传false 
         */
     AdEverSDK.getInstance().exitSinaOverseasSDK(true);

 ```

 2、在海外联运SDK获取到登录成功回调内，调用设置uid接口，具体代码如下：

 ```

 private IEverUserListener userListener = new IEverUserListener() {
        
        @Override
        public void onLogout(Object data) {
            
        }
        
        @Override
        public void onLoginSuccess(EverUser user, Object pt) {

            /**
             * 设置用户uid
             * @params context:上下文
             * @params uid:用户uid
             */
            AdEverSDK.getInstance().setUid(context,user.getUID());
        }

         @Override
        public void onLoginFailed(String error, Object pt) {
           
        }

        @Override
        public void onLoginCancel(Object pt) {
           
        }
 };

 ```