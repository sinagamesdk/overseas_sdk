# 新浪游戏海外融合sdk(安卓)接入文档 

### 1.更新历史

#### 2019-12-27 v1.3.0
新增AF统计接入方式


#### 2019-12-05 v1.2.0

升级到android X版本及融入AF和FB统计

```
1.新浪海外融合SDK初稿
``` 
### 2.前言

此接入文档适用于所有合作方的海外发行安卓游戏。主要内容包括接入方式以及接口调用的方法和参数说明等。

**(若使用AAR引入资源方式)**
cp在接入SDK后可以利用此游戏包测试游戏的主要功能是否正常，随后需要将apk交付我方同事根据渠道要求合出正式游戏包才能测试支付与第三方登录功能；在测试时包名须与谷歌支付后台保持一致，否则支付失效；

**(若使用Modules引用方式)**
cp接入SDK后可以直接打出游戏apk进行完整流程测试，在测试时包名须与谷歌支付后台保持一致，否则支付失效。请在测试流程之前与我方工作人员协调确认测试流程.


### 3.准备工作



**由于谷歌框架的特殊性，本项目暂时仅支持androidstudio引入**

- 导入SDK工程的步骤

#### 3.1 Modules引用工程方式(推荐使用)
![输入图片说明](https://images.gitee.com/uploads/images/2018/0717/180550_0d895eec_518592.png "YC]FAHE0ZBB0ETL@O5HV{]2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0717/180728_ca34aa64_518592.png "G6S01I0(J_E04_BB8UWI6JY.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/1203/142540_58924060_1347645.png "FC$VH}@Z{B(J_SMDS{]L{LN.png")

**打开游戏工程的modules setting，将我方提供的SinagameOverseaSdk的modules模块添加依赖,环境配置请于我方配置文件保持一致，google升级后sdk version必须>=28，即适应android X版本api**

![输入图片说明](https://images.gitee.com/uploads/images/2019/1203/143302_f0586865_1347645.png ")$7YRKNW1MB67IJ~8S`_C9C.png")
**根目录下gradle.properties需配置
android.useAndroidX=true
android.enableJetifier=true 若无gradle.properties则可在相同目录下新建一个即可，请务必确保跟截图目录一致**


#### 3.2 aar资源引用工程方式

**请在将我方提供的8个.aar文件和一个facebook远程引用，aar拷贝至工程的libs目录下即可。随后需要在项目的build.gradle文件内添加第三方aar资源。(注意！在使用aar资源引入方式的时候尽量保持minSdk大于等于15，targetSdk大于等于28，否则工程运行时可能会发生错误)详细请看下图**
 
![所有aar由我方人员提供，build.gradle配置如下](https://images.gitee.com/uploads/images/2019/1205/172147_3518db1f_1347645.png "[~R5$LE4$F7)P_4YVQ(D21N.png")

### 4.开始接入

- 4.1.1 Application相关内容

**请在代码中让游戏工程的application继承application，重写以下三个方法并且在对应方法内分别调用以下方法，若没有正确调用，可能会造成初始化异常**

 ```

//若无application类，请自行创建application并继承application

public class TestApplication extends Application{
    
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        EverSDK.getInstance().attachBaseContext(this);
    }
    
    
    @Override
    public void onCreate() {
        super.onCreate();
        EverSDK.getInstance().onApplicationInit(this);
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        EverSDK.getInstance().onTerminate(this);
    }

```

**请在游戏运行的Activity中每个生命周期方法中加入生命周期相关代码，请务必加入以下代码！**

- 4.1.2 加入生命周期相关的代码

```
/************************ 复制以下代码 ************************/
    @Override
    protected void onPause() {
        super.onPause();
        EverSDK.getInstance().onPause(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        EverSDK.getInstance().onRestart(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EverSDK.getInstance().onResume(this);
    }
    

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        EverSDK.getInstance().onNewIntent(intent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EverSDK.getInstance().onStop(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EverSDK.getInstance().onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EverSDK.getInstance().onActivityResult(this,requestCode,resultCode,data);
    }
}

```


## 4.2设置监听器

#### 系统监听systemListener回调说明

```
    //系统相关的监听器
    private IEverSystemListener systemListener = new IEverSystemListener() {

        @Override
        public void onInitSuccess() {
            //初始化成功的回调
        }

        @Override
        public void onInitFailed(String reason) {
            //初始化失败的回调
        }

        @Override
        public void onExitSuccess() {
           //退出成功的回调,请务必在回调中处理销毁游戏进程的操作
           //例如以下代码
            ActivityManager manager = getSystemService(ACITIVTY_SERVICE);
            manager.killBackgroundProcess(此处参数为游戏的包名);
        }

        @Override
        public void onExitFailed(String reason) {
           //退出失败的回调
        }
    };

```


#### 用户监听器
回调状态|说明|参数说明
--------|-------------|--------|
onLoginSuccess(EverUser user, Object pt)|登录成功|user:新浪用户对象;pt:pt可以是任意值，当登录成功时会异步回调回传给接入方的server
onLoginFailed(String error, Object pt)|登录失败|error:返回的错误信息;pt:同上
onLoginCancel(Object pt)|取消登录|pt:同上
onLogout(Object data)|登出(注销)|data:透传参数

**注意:请让unity层和cocs层在设计的时候保证每个界面都能收到用户监听的回调状态，否则会因为某些渠道SDK的特殊情况无法收到回调通知导致游戏行为阻断**


```
    //用户相关的监听器
    private IEverUserListener userListener = new IEverUserListener() {   
   
     /**
     * 登入成功回调（流程：调用渠道方SDK登入成功，再请求我方服务器进行数据校验，校验成功后返还的user）
     * @param user 校验成功回调生成的user对象
     * @param pt 透传参数
     */
        @Override
        public void onLoginSuccess(EverUser user, Object pt) {
           
        }
     /**
     * 登入失败回调（流程：错误信息来源于渠道SDK，传递到此回调）
     * @param error 错误
     * @param pt 透传参数
     */
        @Override
        public void onLoginFailed(String error, Object pt) {
          
        }
     /**
     * 取消登入回调
     * @param pt 透传参数
     */
    public void onLoginCancel(Object pt);
        @Override
        public void onLoginCancel(Object pt) {
         
        }

     /**
     * 登出回调（调用渠道方的登出）
     * @param data 透传参数
     */
        @Override
        public void onLogout(Object data) {
    
        }
    };

```

#### 支付监听器

回调状态|说明|参数说明
--------|-------------|--------|
onPaySuccess(String orderId)|支付成功|orderId:支付成功返回的订单号
onPayFailed(String orderId, String error)|支付失败|orderId:支付失败返回的订单号;error:失败原因.
onPayCancel(String orderId)|支付取消|orderId:支付取消返回的订单号
```
    //支付相关的监听器
    private IEverPaymentListener payListener = new IEverPaymentListener() {
        
     /**
     * 支付成功回调
     * @param orderId 订单号
     */
        @Override
        public void onPaySuccess(String orderId) {
            
        }
        
    /**
     * 支付失败回调
     * @param orderId 订单号
     * @param error 错误原因
     */
        @Override
        public void onPayFailed(String orderId, String error) {

        }
        
     /**
     * 取消支付回调
     * @param orderId 订单号
     */Id 订单号
     */
        @Override
        public void onPayCancel(String orderId) {

        }
    };


```

#### 4.3初始化SDK

请在游戏运行Activity的onCreate方法中初始化SDK，并且在初始化之前设置好系统相关的systemListener回调监听.(请不要在子线程中调用初始化方法，否则有可能出现未知错误。)请在初始化SDK之后务必加上 EverSDK.getInstance().onCreate(this);这段代码.

方法|说明|必接/选接
--------|-------------|--------|
EverSDK.getInstance().setSystemListener(systemListener)|设置系统监听|必接
EverSDK.getInstance().setUserListener(userListener)|设置用户监听|必接
EverSDK.getInstance().setPayListener(payListener)|设置支付监听|必接
EverSDK.getInstance().initSDK(this)|SDK初始化方法|必接
EverSDK.getInstance().onCreate(this)|请将onCreate方法放在init方法之后执行|必接

- **initSDK方法的回调都在系统监听中处理**

```

@Override
protected void onCreate(Bundle savedInstanceState) {          
     super.onCreate(savedInstanceState);
         //初始化之前相关模块监听器的设置
     EverSDK.getInstance().setSystemListener(systemListener);
     EverSDK.getInstance().setUserListener(userListener);
     EverSDK.getInstance().setPayListener(payListener);
     //调用初始化
     EverSDK.getInstance().initSDK(this);
     //请在初始化SDK之后务必调用onCreate方法
     EverSDK.getInstance().onCreate(this);  
}
```


#### 4.4登录


**接入者调用此方法进行登录操作**


```

EverSDK.getInstance().login(Activity activity);

```


#### 4.4.1 个人中心(必接)

  ##### 显示绑定和切换页面，游戏需根据自己情况在合理位置点击时候调用该方法，由于目前登录默认为游客登录方式，故此方法为必接方法

```
EverSDK.getInstance().personalCenter(Activity activity);

```



 #### 4.4.2 是否登录
**此方法返回一个布尔值用于区分当前用户登录状态.true为已登录,false为未登录**

```

EverSDK.getInstance().isLogin(Activity activity)

```


#### 4.5支付功能接入


请调用以下方法进行支付功能接入，具体代码如下：

4.5.1 实例化GameUser 用户信息类，具体代码如下：

```
 EverUser user = GameInfo.getCurrentEverUser();
 if (user != null) {
       GameUser gameUser = new GameUser();
       gameUser.setUid(user.getUID());
       gameUser.setName(user.getUserName());
 }

```

4.5.2 实例化GameProduct 商品信息类,具体代码如下

```
 GameProduct gameProduct = new GameProduct();
 //订单号  
 gameProduct.setOrderId("orderID:" );
 //金额
 gameProduct.setAmount(99);
 //商品描述
 gameProduct.setProductDesc("物品描述");
 //商品id
 gameProduct.setProductId("shizhifeitwn15");
 //商品名称
 gameProduct.setProductName("物品标题");
 //透传参数
 gameProduct.setPt("透传参数");

```

4.5.3 正式接入:

```

     /**
       * 实际支付方法
       * @params activity:当前类
       * @params gameUser:用户信息
       * @params gameProduct:商品信息
       */
       EverSDK.getInstance().pay(activity, gameUser, gameProduct);
```



#### 5.登录成功返回的EverUser对象说明

- 用户登录相关的Everuser对象为新浪用户对象 详细内容请参考代码注释.EverUser对象仅针对母包渠道

**登录成功回调在海外融合SDK有所变化，EverUser新增了authorizeStatus字段以json形式封装当前账户绑定情况及昵称，未绑定则为null
{“google”:"1","google_name":"nickgoogle1","facebook":"1","facebook_name":"nickfacebook1"}**


返回参数|说明
--------|-------------|
sUid|新浪游戏用户uid
channel|渠道号,默认返回为新浪渠道
token|登录token,对于非新浪渠道包会返回渠道生成的token
userName|新浪游戏用户游戏名,由我方生成
```
public class EverUser extends Cdata {

    /** 新增平台suid对象 10月24日后接入时，请通过Everuser的对象调用getUID方法去获取sUid的值*/
    private String sUid;

    /** 渠道号 */
    private String channel;

    /** 我方根据渠道返还的生成的token */
    private String token;

    /** 我方服务器生成的userName */
    private String userName;

```

### 6.AF统计

**sdk融入了AF统计，以下方法可供接入方调用**

6.1. AF统计
  
    1. AF普通统计（用于接入AF的普通事件统计，eventName需要传入对应的标准事件名称。）

    /**
     * @param activity:当前activity
     * @param eventName:事件名称
     * @param eventValue:预留参数，可为null
     */
    
    EverSDK.getInstance().putAFEventPoint(Activity activity,String eventName,Map<String, Object> eventValue)
    
  
   

### 7.更改配置，参数由我方人员提供

**需要更改module sdk的asssets目录下game_config.properties的appkey、appname以及orientation值**
![输入图片说明](https://images.gitee.com/uploads/images/2020/0821/185915_f0957e54_5553443.png "屏幕截图.png")

**需要更改module sdk对应res目录下的所有语言values server_client_id和facebook_app_id字符串key**
![输入图片说明](https://images.gitee.com/uploads/images/2019/1205/171505_a9871461_1347645.png "P8)I7$8F(COF0$IFC5V})$F.png")



### 8.打包测试

```
keystore密码:android  
key别名:androiddebugkey  
key密码:android  

```
