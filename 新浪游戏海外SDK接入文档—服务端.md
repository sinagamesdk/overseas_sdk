#新浪游戏海外SDK服务端接入文档 

## 1.用户登录
### 1.1.用户验证接口
#### http://overseas.game.weibo.cn/game/distsvc/2/user/serververify
#### 
#### HTTP请求方式
POST/GET
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
uid|string       | sdk用户唯一标识 | true
appkey|string       | 应用的appkey | true
access_token|string       | 用户登录授权 | true
signature|string   | 用于参数校验的签名，生成办法参考1.2 | true

#### 返回参数说明
##### 格式 
JSON
##### 成功返回格式
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
uid|string       | 融合sdk用户唯一标识 | true
access_token|string       | 渠道用户uid | true
##### 示例：
```json
{
    "uid":"123456",
    "access_token":"access_token"
}
```

##### 错误返回格式
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
request|string       | 出错请求 | true
error_code|int       | 错误码(错误码含义见附录) | true
error|string       | 错误描述  | true
##### 示例：
```json
{
    "request": "/game/distsvc/2/user/serververify",
    "error_code": 10025,
    "error": "Sign verify failed"
}
```

### 1.2.用户验证接口签名机制
#### 1、签名规则
```
a)参数signature不参与签名
b)用于计算时参数取值不要做urlencode。
```
#### 2、签名步骤
```
a)将所有待签名参数按参数名排序(字母字典顺序，例如PHP的ksort()函数)
b)把数组所有元素，按照“参数=参数值”的模式用字符'&'拼接成字符串，组成字符串A
c)将字符串A与signature key用英文竖杠'|'进行连接, 得到字符串B，再对字符串B取md5值，得到字符串C，C就是所需要的签名
```
#### 3、示例
```
http://overseas.game.weibo.cn/game/distsvc/2/user/serververify?uid=123456&appkey=123456789&access_token=testtoken&signature=3a7a52861b9872b666c5be6373699ebb
则计算签名值的方法为：
md5("access_token=testtoken&appkey=123456789&uid=123456|bfcc61f0f723d6f1c0659353e4a8b5f3")

/**
 * 生成签名结果
 * @param $para_sort 已排序要签名的数组
 * return 签名结果字符串
 */
public static function buildRequestMysign($signature_key)
{
	if (empty($_REQUEST)) return FALSE;
	if (isset($_REQUEST['signature'])) unset($_REQUEST['signature']);
	//将所有待签名参数按参数名排序
	ksort($_REQUEST);

	//把数组所有元素，按照“参数|参数值”的模式用“|”字符拼接成字符串，组成字符串A
        $str_A = '';
        foreach ($_REQUEST as $key => $value)
        {
            $str_A .= sprintf('%s=%s&', $key, $value);
        }
        $str_A = rtrim($str_A, '&);

	//将字符串A与signature_key，用英文竖杠进行连接, 得到字符串B
	$str_B = sprintf('%s|%s', $str_A, $signature_key);

	//对字符串B取md5值，得到字符串C，C就是所需要的签名
	$str_C = md5($str_B);

	return $str_C;
}
```
### 1.3.本地token校验方式
因网络原因导致服务器连接经常出现失败或者超时时可对access_token进行本地校验，不调用1.1的接口
#### 1、校验规则
```
将uid、access_token去掉后6位后的字符串、signature_key用'|'符号连接，进行md5，md5值的后6位应当与access_token的后6位一致
```
#### 2、示例
```
uid=123456
access_token=testtoken088301
校验结果为substr(md5("123456|testtoken|bfcc61f0f723d6f1c0659353e4a8b5f3"), -6)="088301"
```

## 2.支付接口
### 2.1.回调接口由开发商提供
与上述各接口不同，回调接口由各家CP各自进行开发和部署，遵循一致的规范，供手机微游戏调用。
#### 接口加密密钥，请使用游戏的 appsecret
#### HTTP请求方式
GET
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
status|int       | 订单状态 1为成功 | true
uid|string       | sdk用户唯一标识  | true
appkey|string|应用的appkey | true
order_id|string       | sdk订单id | true
amount|int      | 支付金额，单位 分  | true
actual_amount|int      | 实际支付金额，单位 分 | true
cpext|string       | CP自定义参数 | false
cp_order_id |string|CP订单id | false
updatetime|string|通知时间 | true
test|int    |是否是测试或沙盒订单 1为测试订单 0为正式订单
sign|string   |用于参数校验的签名，生成办法参考2.2 | true

#### 返回参数说明
```
http状态码应为200，返回结果为字符串OK，且必须是大写。
```
#### 测试示例

### 2.2.支付接口签名机制
#### 1、签名规则
```
a)参数signature不参与签名
b)用于计算时参数取值不要做urlencode。

```

#### 2、签名步骤
```
a)将signature外所有参数按参数名排序(字母字典顺序，例如PHP的ksort函数)。
b)把数组所有元素，按照“参数|参数值”的模式用“|”字符拼接成字符串，组成字符串A
c)将字符串A与 appsecret，用英文竖杠进行连接, 得到字符串B，对字符串B取sha1值，得到字符串C，C就是所需要的签名
```
#### 3、示例
```
http://test.game.weibo.cn/paysys/pay.php?status=1&uid=user00001&appkey=123456789&amount=1&actual_amount=1&order_id=order_0001&cpext=1&cp_order_id=cp_order_0001&updatetime=1970-01-01 00:00:00&sign=18844df00fff10aacc1d26ba2d18053d05e2341a
则计算签名值的方法为：
sha1(“actual_amount|1|amount|1|appkey|123456789|cp_order_id|cp_order_0001|cpext|1|order_id|order_0001|status|1|uid|user00001|updatetime|1970-01-01 00:00:00|test_appsecret”)

/**
 * 生成签名结果
 * @param $para_sort 已排序要签名的数组
 * return 签名结果字符串
 */
public static function buildRequestMysign($appsecret)
{
	if (empty($_REQUEST)) return FALSE;
	if (isset($_REQUEST['sign'])) unset($_REQUEST['sign']);
	//将所有待签名参数按参数名排序
	ksort($_REQUEST);

	//把数组所有元素，按照“参数|参数值”的模式用“|”字符拼接成字符串，组成字符串A
    $str_A = '';
    foreach ($_REQUEST as $key => $value)
    {
        $str_A .= sprintf('%s|%s|', $key, $value);
    }

	//将字符串A与appsecret，用英文竖杠进行连接, 得到字符串B
	$str_B = $str_A . $appsecret;

	//对字符串B取sha1值，得到字符串C，C就是所需要的签名
	$str_C = sha1($str_B);

	return $str_C;
}
```
## 3.查询接口
### 3.1.退款查询接口
#### http://overseas.game.weibo.cn/game/distsvc/2/api/query_voidpurchase
#### 
#### HTTP请求方式
GET
#### 提交参数说明
参数字段 | 类型|说明 | 必选
--------|-------------|--------|---
appkey  |string       | 应用的appkey | true
paytype |integer       | 支付方式，谷歌1，苹果2 | false
start_id|integer       | 查询的最小id | false
start_time |string       | 退款时间开始时间 | false
end_time |string       | 退款时间结束时间 | false
signature|string   | 用于参数校验的签名，生成办法参考1.2 | true

#### 返回参数说明
##### 格式 
文本格式，第一行为列名
##### 成功返回格式
id，退款表id，对应start_id  <br>
orderid，回调接口中返回的order_id  <br>
item，下单道具  <br>
paytime，支付时间  <br>
void_time，退款时间，对应参数中start_time和end_time
```
id    appkey    paytype    uid    orderid    amount    item    paytime    voidtime
123    123456    1    1234567    4312    499    test_item    2021-03-04 13:04:26    2021-03-16 21:00:05
```

## 附录
### 登录验证错误码
错误码 | 含义
------|-------
10001 | 参数错误
10012 | 获取用户信息失败
10013 | access_token失效
10025 | 签名验证失败
